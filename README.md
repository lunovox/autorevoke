![favicon]

# 🚫 Privilege Autorevoker

[Minetest MOD] The object of this mod is to automatically revoke some privileges from old normal players (which are not servers).

## 🚫 Licence GNU LGPL:*

* [LICENSE.txt] 
* in wiki to [portuguese][GNU-LGPL.pt] 

## 🚫 Download:

   https://gitlab.com/lunovox/autorevoke

## 🚫 Depends

    -== nothing ==-

## 🚫 Optional Depends 

    -== nothing ==-

## 🚫 Author

* Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:mastodon.social/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [Mumble](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## 🚫 How to configure:

Edit the ```init.lua``` file on the lines...

```lua
local revoked_privs = {
	"home", 
	--"god",
}
```
In this example above it will remove the ```home``` privilege from all normal players (those who don't have the ```server``` privilege). Obviously the ```serve``` is the only one that will not be automatically removed by this mod, and for that reason this mod doesn't work in singleplayer mode. The double quote symbol (```"```) is required. Note that the ```god``` privilege will not be taken away from players because of the ```--``` at the beginning of the line.

[favicon]:https://gitlab.com/lunovox/autorevoke/-/raw/main/favicon.jpg
[LICENSE.txt]:https://gitlab.com/lunovox/autorevoke/-/raw/main/LICENSE.txt
[GNU-LGPL.pt]:https://pt.wikipedia.org/wiki/GNU_Lesser_General_Public_License
