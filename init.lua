--[[
	The object of this mod is to automatically revoke some privileges from old normal players (which are not servers).
	Translate to Portuguese: O objeto deste mod é automaticamente revogar alguns privilégios dos jogadores normais antigos (que não são servidores).
--]]


local revoked_privs = { --Table of Revoked Privs 
	"home", 
	--"god", --Zueira
}

minetest.register_on_joinplayer(function(player) 
	local playername = player:get_player_name()
	local privs = minetest.get_player_privs(playername) 
	--minetest.chat_send_all("[AUTOREVOKE] "..playername..": "..dump2(privs)) 
	if 
		type(privs.server) == "nil" 
		or privs.server ~= true
		--or 1==1
	then
		for myIndex, myPrivRevoked in ipairs(revoked_privs) do
			if 
				type(myPrivRevoked) == "string"
				and myPrivRevoked:trim() ~= ""
			then
				myPrivRevoked = myPrivRevoked:trim()
				if 
					type(privs[myPrivRevoked]) == "boolean" 
					and privs[myPrivRevoked] == true 
				then
					privs[myPrivRevoked] = nil
					minetest.set_player_privs(playername, privs)
					minetest.chat_send_all("[".. core.colorize("#00FF00", "AUTOREVOKE").."] The privilege "..core.colorize("#FF0000", myPrivRevoked).." of player "..core.colorize("#FFFF00", playername).." has been automatically revoked by the system!") 
				end
			end
		end	
	end
end)

